# LikeAnimation

本项目是基于开源项目frogermcs/LikeAnimation进行harmonyos化的移植和开发的，可以通过[github地址](https://github.com/frogermcs/LikeAnimation) 追踪到原项目.

## 项目介绍
### 项目名称：LikeAnimation
### 所属系列：harmonyos的第三方组件适配移植
### 功能：仿造实现Twitter心形点赞按钮
### 调用差异：无
### 原项目地址：https://github.com/frogermcs/LikeAnimation

## 安装教程

Repository
Add this in your root build.gradle file (not your module build.gradle file):
```groovy
allprojects {
	repositories {
		...
		mavenCentral()
	}
}
```

Dependency
Add this to your module's build.gradle file:

```groovy
dependencies {
    ...
    implementation 'com.gitee.ts_ohos:like_animation:1.0.2'
}
```

## 使用说明
在您的布局文件中添加如下代码：
```xml
<frogermcs.io.likeanimation.widget.LikeButtonView
        ohos:id="$+id:btn_star"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:layout_alignment="center"
        app:starSize="100vp"/>
```

## 方法

| 方法名                                          | 返回类型 | 描述                     |
| ----------------------------------------------- | -------- | ------------------------ |
| setOnStarCheckedListener(OnStarCheckedListener) | void     | 添加按钮是否被选中的监听 |

## AttrSet属性

| AttrSet  | format    | describe                 |
| -------- | --------- | ------------------------ |
| starSize | dimension | 设置按钮的大小，默认50vp |

## 效果演示
![Circle animation](http://frogermcs.github.io/images/22/button_anim.gif "Circle animation")

This is an example code described in blog post: [Twitter's like animation in Android - alternative](http://frogermcs.github.io/twitters-like-animation-in-android-alternative/).

## 移植版本
Branches/master(0aa95cc on 17 Apr 2018)

## 版本迭代

v1.0.2 调整控件默认大小

v1.0.1 增加修改控件大小和是否选中的回调监听

v1.0.0 首次提交移植版本

##  License

```
Copyright 2015 Miroslaw Stanek

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
