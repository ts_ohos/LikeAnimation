package frogermcs.io.likeanimation.slice;

import frogermcs.io.likeanimation.ResourceTable;
import frogermcs.io.likeanimation.widget.LikeButtonView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice {

    private LikeButtonView likeButtonView;
    private Text text;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initView() {
        likeButtonView = (LikeButtonView) findComponentById(ResourceTable.Id_btn_star);
        text = (Text) findComponentById(ResourceTable.Id_text_result);
        likeButtonView.setOnStarCheckedListener(new LikeButtonView.OnStarCheckedListener() {
            @Override
            public void onChecked(boolean isChecked) {
                if (isChecked) {
                    text.setText("On");
                } else {
                    text.setText("Off");
                }
            }
        });
    }
}
